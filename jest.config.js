module.exports = {
  moduleNameMapper: {
    "\\.css$": "identity-obj-proxy"
  },
  setupFiles: ["<rootDir>/enzyme.config.js"]
};
