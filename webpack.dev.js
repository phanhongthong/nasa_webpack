var webpack = require("webpack");
var path = require("path");

var merge = require("webpack-merge");
var common = require("./webpack.common");

var BUILD_DIR = path.join(__dirname, "dist");

var config = {
  mode: "development",
  output: {
    path: path.resolve(__dirname, "dist"),
    filename: "main.js",
    publicPath: "/"
  },
  devServer: {
    contentBase: BUILD_DIR,
    compress: true
  },
  plugins: [
    new webpack.DefinePlugin({
      "process.env.NODE_ENV": JSON.stringify(process.env.NODE_ENV)
    })
  ]
};

module.exports = merge(common, config);
